package nl.bioinf.junidemo;

public class Util {

    public static void main(String[] args) {
        Util u = new Util();
        try{
            u.calculateSurface(0, 0);
        } catch (IllegalArgumentException ex) {
            //doe iets
        }
    }

    /**
     * Calculates the surface for given metrics.
     * @param length the length in meters
     * @param width the width in meters
     * @return the surface in meters squared
     */
    public static double calculateSurface(double length, double width) {
        if (length < 0 || width < 0) {
            throw new IllegalArgumentException("Length or width is below zero!");
        }
        return length * width;
    }
}
