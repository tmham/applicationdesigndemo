package nl.bioinf.junit_exercise;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * returns the Euclidean distance
     * @param other
     * @return
     */
    public double distanceTo(Point other){
        if (other == null) {
            throw new IllegalArgumentException("null input provided for Point");
        }
        double distance = Math.sqrt(Math.pow(this.x - other.x, 2) +
                Math.pow(this.y - other.y, 2));
        return distance;
    }
}
