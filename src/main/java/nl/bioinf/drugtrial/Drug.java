package nl.bioinf.drugtrial;

public interface Drug {
    int getResponse();
}
