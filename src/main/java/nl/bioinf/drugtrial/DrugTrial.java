package nl.bioinf.drugtrial;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DrugTrial {
    private List<Subject> subjects = new ArrayList<>();

    public void addSubject(Subject subject) {
        //what if subject == null????
        if (null == subject) {
            throw new IllegalArgumentException("subject cannot be null");
        }
        subjects.add(subject);
    }

    public List<Subject> getSubjects() {
        return Collections.unmodifiableList(subjects);
    }

    public void performTrial() {
        for (Subject subject : subjects) {
            subject.getResponse(getRandomDrug());
        }
    }

    private Drug getRandomDrug() {
        final Drug[] drugs = new Drug[2];
        drugs[0] = new Placebo();
        drugs[1] = new RealDrug();
        int index = (int)(Math.random() * 2);
        return drugs[index];
    }
}
