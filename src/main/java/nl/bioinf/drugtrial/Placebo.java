package nl.bioinf.drugtrial;

public class Placebo implements Drug {
    @Override
    public int getResponse() {
        return 5;
    }
}
