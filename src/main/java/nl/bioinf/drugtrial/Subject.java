package nl.bioinf.drugtrial;

public class Subject {
    private String id;

    public Subject(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getResponse(Drug drug) {
        //delegation!
        return drug.getResponse();
    }
}
